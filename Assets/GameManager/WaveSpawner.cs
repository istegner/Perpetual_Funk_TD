﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WaveSpawner : MonoBehaviour
{
    public static int enemiesAlive = 0;

    public Wave[] waves; //The waves
    public Transform spawnPoint;

    public float timeBetweenWaves; //Time between waves
    private float countdown; //Time until next wave

    private int waveNumber; //The wave number

    public Text waveCountdownText; //The UI element to display the time unil the next wave
    public Text waveNumberText; //The UI element to display the time until the next wave

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        waveNumberText.text = "Wave " + waveNumber;
        //Debug.Log(enemiesAlive);
        //If there are still enemies, do nothing
        if (enemiesAlive > 0)
        {
            return;
        }

        if (waveNumber == waves.Length)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            this.enabled = false;
        }

        //If the countdown until the next wave is over, start the next wave and reset the timer
        if (countdown <= 0)
        {
            StartCoroutine(SpawnWave());
            countdown = timeBetweenWaves;
            return;
        }

        countdown -= Time.deltaTime;
        countdown = Mathf.Clamp(countdown, 0f, Mathf.Infinity);
        //Print the time until the next wave in the format 0:00.00
        waveCountdownText.text = string.Format("{0:00.00}", countdown);
    }

    /// <summary>
    /// Start the next wave
    /// If finished the last wave, the player won
    /// </summary>
    IEnumerator SpawnWave()
    {
        Wave wave = waves[waveNumber];
        for (int i = 0; i < wave.count; i++)
        {
            SpawnEnemy(wave.enemies);
            yield return new WaitForSeconds(1f / wave.rate);
        }
        waveNumber++;
    }

    //Spawn a random enemy
    void SpawnEnemy(GameObject[] enemy)
    {
        Instantiate(enemy[Random.Range(0, enemy.Length)], spawnPoint.position, spawnPoint.rotation);
        enemiesAlive++;
    }
}
