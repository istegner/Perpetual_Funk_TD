﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSControlls : MonoBehaviour
{
    public bool basicFPS = false;
    public bool advancedFPS = false;

    public GameObject basicFPS_GO;
    public GameObject advancedFPS_GO;

    // Update is called once per frame
    void Update()
    {
        basicFPS_GO.SetActive(basicFPS);
        advancedFPS_GO.SetActive(advancedFPS);
    }
}
