﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public float startSpeed;
    [Tooltip("The amount of damage is dealt when at the core")]
    public float coreDamage;
    public float startHealth;
    public float sensitivity = 0.2f;
    [HideInInspector]
    public float speed;

    public int deathMoney;

    private float health;

    public GameObject deathEffect;
    public GameObject[] enemiesToSpawn;

    public Image healthBar;

    // Use this for initialization
    void Start()
    {
        speed = startSpeed;
        health = startHealth;
    }

    public void TakeDamage(float damage)
    {
        health -= damage;
        healthBar.fillAmount = health / startHealth;
        if (health <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        WaveSpawner.enemiesAlive--;
        if (this.gameObject.name.Contains("Squad"))
        {
            this.GetComponent<RoboSquadVehicleDeath_Script>().RoboSquadVehicleDeath(this.transform, this.GetComponent<EnemyMovement>().target);
            //for (int i = 0; i < enemiesToSpawn.Length; i++)
            //{
            //    Debug.Log("Spawning enemy " + i + " of " + enemiesToSpawn.Length);
            //    GameObject enemy = (GameObject)Instantiate(enemiesToSpawn[i], transform.position, transform.rotation);
            //    enemy.GetComponent<EnemyMovement>().target = this.GetComponent<EnemyMovement>().target;
            //    WaveSpawner.enemiesAlive++;
            //}
        }
        PlayerStats.money += deathMoney;
        GameObject effect = (GameObject)Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(effect, 5f);
        Destroy(gameObject);
    }

    /// <summary>
    /// When the enemy is hit with the slow laser
    /// </summary>
    public void Slow(float slowFactor)
    {
        speed = startSpeed * (1f - slowFactor);
    }
}
