﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Enemy))]
public class EnemyMovement : MonoBehaviour
{
    [HideInInspector]
    public Transform target;

    private int wavepointIndex = 0;

    private Enemy enemy;

    GameObject gameManager;

    public GameObject nonPlaceHolderModel;

    // Use this for initialization
    void Start()
    {
        enemy = GetComponent<Enemy>();
        target = Waypoints.waypoints[0];
        gameManager = GameObject.FindGameObjectWithTag("GameManager");
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * enemy.speed * Time.deltaTime, Space.World);
        nonPlaceHolderModel.transform.LookAt(target);

        if (Vector3.Distance(transform.position, target.position) <= enemy.sensitivity)
        {
            GetNextWaypoint();
        }

        enemy.speed = enemy.startSpeed;
    }

    void GetNextWaypoint()
    {
        if (wavepointIndex >= Waypoints.waypoints.Length - 1)
        {
            EndPath();
            return;
        }

        wavepointIndex++;
        target = Waypoints.waypoints[wavepointIndex];
    }

    /// <summary>
    /// The enemy has reached the end of the path
    /// </summary>
    void EndPath()
    {
        WaveSpawner.enemiesAlive--;
        gameManager.GetComponent<PlayerStats>().TakeDamage(enemy.coreDamage);
        Destroy(gameObject);
    }
}
