﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{
    public GameObject pauseMenu;
    public GameObject pauseButton;
    public GameObject mainUI;

    public void StartButton()
    {
        SceneManager.LoadScene(1);
    }

    public void MenuButton()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(0);
    }

    public void LevelOne()
    {
        SceneManager.LoadScene(1);
    }

    public void LevelTwo()
    {
        SceneManager.LoadScene(2);
    }

    public void LevelThree()
    {
        SceneManager.LoadScene(3);
    }


    /// <summary>
    /// Activate the pause menu and deactivate the pause button
    /// Set the timeScale to 0 to "pause" the game
    /// </summary>
    public void Pause()
    {
        pauseMenu.SetActive(true);
        pauseButton.SetActive(false);
        mainUI.SetActive(false);
        Time.timeScale = 0f;
    }

    /// <summary>
    /// Deactivate the pause menu and activate the pause button
    /// Reset the timeScale back to 1
    /// </summary>
    public void Resume()
    {
        pauseMenu.SetActive(false);
        pauseButton.SetActive(true);
        mainUI.SetActive(true);
        Time.timeScale = 1f;
    }
}
