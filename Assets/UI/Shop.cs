﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    [Header("Turret Blueprints")]
    public TurretBlueprint laserTurret;
    public TurretBlueprint rubiksLauncher;

    [Header("Text's to show cost")]
    public Text laserTurretCost;
    public Text rubiksLauncherCost;

    BuildManager buildManager;

    private void Start()
    {
        buildManager = BuildManager.instance;
        laserTurretCost.text = "$" + laserTurret.cost.ToString();
        rubiksLauncherCost.text = "$" + rubiksLauncher.cost.ToString();
    }

    public void SelectLaserTurret()
    {
        Debug.Log("Laser Turret Selected");
        buildManager.SelectTurretToBuild(laserTurret);
    }

    public void SelectRubiksLauncher()
    {
        Debug.Log("Selected Rubiks Cube Launcher");
        buildManager.SelectTurretToBuild(rubiksLauncher);
    }
}
