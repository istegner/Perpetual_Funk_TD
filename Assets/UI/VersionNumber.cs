﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VersionNumber : MonoBehaviour
{
    public Text versionNumber;

    // Use this for initialization
    void Start()
    {
        versionNumber.text = "v" + Application.version;
    }
}
