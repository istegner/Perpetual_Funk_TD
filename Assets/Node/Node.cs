﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Node : MonoBehaviour
{
    public float colourChangeDelay;

    private Renderer rend;

    private Color colour;

    public Vector3 positionOffset;

    public Color[] colourList;

    [HideInInspector]
    public GameObject turret;
    [HideInInspector]
    public TurretBlueprint turretBlueprint;
    [HideInInspector]
    public bool isUpgraded = false;
    [HideInInspector]
    public Light light;

    BuildManager buildManager;

    // Use this for initialization
    void Start()
    {
        rend = GetComponent<Renderer>();

        buildManager = BuildManager.instance;
        light = GetComponentInChildren<Light>(); //Get the Light component of the light
        StartCoroutine(ChangeColour());
    }

    IEnumerator ChangeColour()
    {
        colour = colourList[Random.Range(0, colourList.Length)];
        light.color = colour; //Change the light colour to the same as the node colour
        rend.material.color = colour;
        yield return new WaitForSeconds(colourChangeDelay);
        StartCoroutine(ChangeColour());
    }

    void BuildTurret(TurretBlueprint blueprint)
    {
        if (PlayerStats.money < blueprint.cost)
        {
            Debug.Log("Not eneough money");
            return;
        }
        turretBlueprint = blueprint;
        PlayerStats.money -= blueprint.cost;
        GameObject _turret = (GameObject)Instantiate(blueprint.prefab, GetBuildPosition(), Quaternion.identity);
        turret = _turret;
        GameObject effect = (GameObject)Instantiate(buildManager.buildEffect, GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 5f);
        buildManager.turretToBuild = null;
    }

    public Vector3 GetBuildPosition()
    {
        //Debug.Log(transform.position);
        //Debug.Log(transform.position + positionOffset);
        return transform.position + positionOffset;
    }

    void OnMouseUp()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        if (turret != null)
        {
            buildManager.SelectNode(this);
            return;
        }
        if (!buildManager.CanBuild)
        {
            return;
        }
        BuildTurret(buildManager.GetTurretToBuild());
    }

    public void UpgradeTurret()
    {
        if (PlayerStats.money < turretBlueprint.upgradeCost)
        {
            Debug.Log("Not eneough money to upgrade");
            return;
        }
        PlayerStats.money -= turretBlueprint.upgradeCost;
        //Get rid of old turret
        Destroy(turret);
        //Build new one
        GameObject _turret = (GameObject)Instantiate(turretBlueprint.upgradedPrefab, GetBuildPosition(), Quaternion.identity);
        turret = _turret;
        GameObject effect = (GameObject)Instantiate(buildManager.buildEffect, GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 5f);
        isUpgraded = true;
    }

    public void SellTurret()
    {
        isUpgraded = false;
        PlayerStats.money += turretBlueprint.GetSellAmount();
        GameObject effect = (GameObject)Instantiate(buildManager.buildEffect, GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 5f);
        Destroy(turret);
        turretBlueprint = null;
    }
}