﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceableNodeIdentifier : MonoBehaviour 
{
	public float colourChangeDelay;

	private Renderer rend;
    //private Material mat;

	private Color colour;

    public bool isBlack = false;

	// Use this for initialization
	void Start () 
	{
		rend = GetComponent<Renderer>();
        //mat = GetComponent<Material>();
        //if(mat == null)
        //{
        //    Debug.Log("No material");
        //}
		StartCoroutine (ChangeColour ());
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	IEnumerator ChangeColour()
	{
        //Debug.Log(colour);
        rend.material.color = colour;
        //mat.SetColor("_EmissionColor", colour);
        isBlack = !isBlack;
		yield return new WaitForSeconds (colourChangeDelay);
        StartCoroutine(ChangeColour());
    }
}

