﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    private Transform target;
    private Enemy targetEnemy;

    [Header("General")]
    [Tooltip("The range of the turret")]
    public float range;

    [Header("Use Bullets (default)")]
    [Tooltip("How fast the turret will shoot")]
    public float fireRate;

    private float fireCountdown;

    [Tooltip("The bullet prefab")]
    public GameObject bulletPrefab;

    [Header("Use laser")]
    public bool useLaser = false;
    public ParticleSystem laserParticle;
    public ParticleSystem impactEffect;
    public Light impactLight;
    public float damageOverTime;
    public float slowFactor;

    [Header("Use shotgun")]
    public bool useShotgun = false;
    public int numberOfBullets = 3;
    public float fireDelay;
    public GameObject shotgunBullet;

    [Header("Unity Setup Fields")]
    [Tooltip("The tag of the enemy")]
    public string enemyTag = "Enemy";
    [Tooltip("How fast the turret will turn to face the enemy")]
    public float turnSpeed = 10f;

    [Tooltip("The pivot point of the turret")]
    public Transform pivotPoint;
    [Tooltip("Where to instantiate the bullets")]
    public Transform firePoint;

    // Use this for initialization
    void Start()
    {
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
    }

    /// <summary>
    /// Find the closest enemy and set it as the target
    /// </summary>
    void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
            targetEnemy = nearestEnemy.GetComponent<Enemy>();
        }
        else
        {
            target = null;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            if (useLaser)
            {
                //if (laserParticle.isPlaying)
                //{
                laserParticle.gameObject.SetActive(false);
                impactEffect.Stop();
                impactLight.gameObject.SetActive(false);
                //}
            }
            return;
        }

        LockOnTarget();

        if (useLaser)
        {
            Laser();
        }
        else
        {
            if (fireCountdown <= 0)
            {
                Shoot();
                fireCountdown = 1f / fireRate;
            }

            fireCountdown -= Time.deltaTime;
        }
    }

    void Laser()
    {
        targetEnemy.TakeDamage(damageOverTime * Time.deltaTime);
        targetEnemy.Slow(slowFactor);
        if (!laserParticle.isPlaying)
        {
            laserParticle.gameObject.SetActive(true);
            impactEffect.Play();
            impactLight.enabled = true;
        }
        //laserParticle(0, firePoint.position);
        //lineRenderer.SetPosition(1, target.position);
        Vector3 dir = firePoint.position - target.position;
        laserParticle.transform.position = firePoint.position + dir.normalized;
        laserParticle.transform.rotation = Quaternion.LookRotation(-dir);

        impactEffect.transform.position = target.position + dir.normalized;
        impactEffect.transform.rotation = Quaternion.LookRotation(dir);
    }

    void Shoot()
    {
        if (useShotgun)
        {
            StartCoroutine(Shotgun(fireDelay));
            return;
        }
        GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Bullet bullet = bulletGO.GetComponent<Bullet>();
        if (bullet != null)
        {
            bullet.Seek(target);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }

    void LockOnTarget()
    {
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(pivotPoint.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        pivotPoint.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }

    IEnumerator Shotgun(float delay)
    {
        for (int i = 0; i < numberOfBullets; i++)
        {
            GameObject bulletGO = (GameObject)Instantiate(shotgunBullet, firePoint.position, firePoint.rotation);
            Destroy(bulletGO, 10f);
            yield return new WaitForSeconds(delay);
        }
    }
}
